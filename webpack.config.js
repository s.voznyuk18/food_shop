const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const mode = process.env.NODE_ENV || 'development';

const devMode = mode === 'development';


module.exports = {
  mode: 'development',
  resolve: {
    alias: {
      ActionsRoot: path.resolve(__dirname, './src/actions'),
      ConfigsRoot: path.resolve(__dirname, './src/configs'),
      ComponentsRoot: path.resolve(__dirname, './src/components'),
      AssetsRoot: path.resolve(__dirname, './src/assets'),
      UtilsRoot: path.resolve(__dirname, './src/utils'),
      ApiRoot: path.resolve(__dirname, './src/api'),
      StyledComponentsRoot: path.resolve(__dirname, './src/styledComponents'),
      ContainersRoot: path.resolve(__dirname, './src/containers') 
    },
    extensions: ['.js', '.jsx', '.json']
  },
  entry: ["@babel/polyfill", "./src/index.js"],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  devServer: {
    historyApiFallback: true,
  },
  plugins: [
    new HTMLWebpackPlugin({template: "./public/index.html"}),
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css'
  })
  ],
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              ['@babel/preset-env', { targets: "defaults" }]
            ]
          }
        }
      },
      {
        test: /\.m?jsx$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              ["@babel/preset-react", '@babel/preset-env']
            ]
          }
        }
      },
      {
        test: /\.(c|sa|sc)ss$/i,
        use: [
            MiniCssExtractPlugin.loader, 
            "css-loader",
            {
                loader: 'postcss-loader',
                options: {
                    postcssOptions: {
                        plugins: [require('postcss-preset-env')]
                    }
                }
            }
        ],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-url-loader',
            options: {
              limit: 10000,
            },
          },
        ],
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
    ]
  }
};