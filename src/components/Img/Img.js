import React, { memo } from "react";
import PropTypes from 'prop-types';

import { Icon } from "./StyledComponents";

const Img = ({ src, alt, width, height, margin, style }) => {
    return (
        <Icon
            style={style}
            src={src}
            alt={alt}
            width={width}
            height={height}
            margin={margin}
        />
    );
}

Img.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    margin: PropTypes.string,
    style: PropTypes.object
}

Img.defaultProps = {
    src: '',
    alt: '',
    width: '',
    height: '',
    margin: '',
    style: {}
}

export default memo(Img);