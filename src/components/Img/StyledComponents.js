import styled from "styled-components";

export const Icon = styled.img`
    width: ${props => props.width};
    height: ${props => props.height};
    margin: ${props => props.margin};
`;