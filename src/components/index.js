import BassicButton from './BassicButton/BassicButton';
import BassicInput from './BassicInput/BassicInput';
import Img from './Img/Img';
import SVG from './SVG/SVG';
import RoutesSection from './RoutesSection/RoutesSection';

export {BassicButton, BassicInput, Img, SVG, RoutesSection};
