import styled from 'styled-components';

export const Wrapper = styled.div`
    display: inline-flex;
    flex-direction: column;
    margin: ${props => props.margin};
    width: ${props => props.width};
`;

export const Label = styled.div`
    display: inline;
    font-size: ${props => props.labelFontSize};
    margin-bottom: ${props => props.labelMarginBottom};
    line-height: ${props => props.labelLineHeight};
    font-weight: 600;
`;

export const InputWrap = styled.div`
    position: relative;
    height: ${props => props.height};
    width: ${props => props.width};
    border-style: hidden;

`;

export const Input = styled.input`
    width: 100%;
    height: 100%;
    padding: ${props => props.padding};
    border: solid 2px ${props => props.borderColor};
    border-radius: ${props => props.borderRadius ? props.borderRadius  : '15px'};
    font-size: ${props => props.fontSize}
`;