import React, { memo } from "react";
import PropTypes from 'prop-types';
import {Img} from 'ComponentsRoot';
import { Wrapper, Label, Input, InputWrap } from './StyledComponents';



import { colors } from "ConfigsRoot/colors";
import searchSvg from 'AssetsRoot/img/svg/search.svg';


const getColor = (error, dirtyFields) => {
    if (error) return colors.errorMessage;
    if (!error && dirtyFields) return colors.validColor;
    return colors.primary;
}

const BassicInput = ({ label, htmlFor, labelFontSize, labelMarginBottom, labelLineHeight, id, type, name, placeholder, width, height, padding, margin, borderRadius, register, validation, dirtyFields, errorMessage, errorMessagemargin, errorFontSize, value, fontSize, searchInput }) => {
    return (
        <Wrapper width={width} margin={margin}>
            <If condition={label}>
                <Label
                    htmlFor={htmlFor}
                    labelFontSize={labelFontSize}
                    labelMarginBottom={labelMarginBottom}
                    labelLineHeight={labelLineHeight}
                >
                    {label}
            </Label>
            </If>
            <InputWrap
                width={width}
                height={height}
            >
                <Input
                    id={id}
                    type={type}
                    name={name}
                    placeholder={placeholder}
                    // value={value}
                    padding={padding}
                    borderRadius={borderRadius}
                    borderColor={getColor(errorMessage, dirtyFields)}
                    fontSize={fontSize}
                    // {...register(name, validation)}
                />
                <If condition={searchInput}>
                    <Img
                        src={searchSvg} 
                        width='25px' 
                        height='25px' 
                        alt='search'
                        style={{
                            position: 'absolute',
                            top: '50%',
                            right: '8px',
                            transform: 'translateY(-50%)'

                        }}
                    />
                </If>
       
            </InputWrap>
          
            {/* <ErrorMessage errorMessagemargin={errorMessagemargin} errorFontSize={errorFontSize} >{errorMessage}</ErrorMessage> */}
        </Wrapper>
    )
};

BassicInput.propTypes = {
    label: PropTypes.string,
    htmlFor: PropTypes.string,
    labelFontSize: PropTypes.string,
    labelMarginBottom: PropTypes.string,
    labelLineHeight: PropTypes.string,
    id: PropTypes.string,
    type: PropTypes.string,
    name: PropTypes.string,
    placeholder: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    padding: PropTypes.string,
    margin: PropTypes.string,
    borderRadius: PropTypes.string,
    // register: PropTypes.func,
    // validation: PropTypes.objectOf({
    //     required: PropTypes.string,
    //     minLength: PropTypes.object,
    //     pattern: PropTypes.object,
    //     min: PropTypes.object
    // }).isRequired,
    dirtyFields: PropTypes.bool,
    errorMessage: PropTypes.string,
    errorMessagemargin: PropTypes.string,
    errorFontSize: PropTypes.string,
    value: PropTypes.string,
    fontSize: PropTypes.string,
    searchInput: PropTypes.bool
};

BassicInput.defaultProps = {
    label: '',
    htmlFor: '',
    labelFontSize: '16px',
    labelMarginBottom: '',
    labelLineHeight: '',
    id: '',
    type: '',
    name: '',
    placeholder: '',
    width: '',
    height: '',
    padding: '',
    margin: '',
    borderRadius: '',
    // register: () => { },
    dirtyFields: false,
    errorMessage: '',
    errorMessagemargin: '',
    errorFontSize: '',
    // value: '',
    fontSize: '16px',
    searchInput: false
}

export default memo(BassicInput);