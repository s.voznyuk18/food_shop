import React from "react";
import { Routes, Route } from "react-router-dom";

import { Main, Contacts, About, Layout, Catalog} from 'ContainersRoot';

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Layout/>}>
          <Route index element={<Main/>}/>
          <Route path="catalog" element={<Catalog/>}/>
          <Route path="about" element={<About/>}/>
          <Route path="contacts" element={<Contacts/>}/>
        </Route>
      </Routes>
    </>
  );
}

export default App;
