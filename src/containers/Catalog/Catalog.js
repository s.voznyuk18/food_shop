import React from "react";


import {Container, Title} from 'StyledComponentsRoot';


import {CatalogSection, CatalogContent, CatalogItem } from './StyledComponents';

const Catalog = () => {
    return (
        <CatalogSection>
            <Container>
                <Title>Catalog</Title>
                {/* <CatalogContent>
                    <CatalogItem></CatalogItem>
                </CatalogContent> */}
            </Container>

        </CatalogSection>
    );
}

export default Catalog;