import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import About from "./About/About";
import Contacts from "./Contacts/Contacts";
import Main from "./Main/Main";
import Layout from './Layout/Layout';
import Catalog from './Catalog/Catalog';

export {
    Header,
    Footer,
    About,
    Contacts,
    Main,
    Layout,
    Catalog
};