import React from "react";
import { Outlet } from "react-router-dom";

import {Header, Footer} from 'ContainersRoot';
import {RoutesSection} from 'ComponentsRoot';

const Layout = () => {
    return (
        <>
            <Header/>
            <RoutesSection/>
            <Outlet/>
            <Footer/>
        </>
    );
}

export default Layout;