import styled from "styled-components";
import { Link } from "react-router-dom";


import { colors } from "ConfigsRoot/colors";

import footerBg from 'AssetsRoot/img/footerBg.png';

export const FooterSection = styled.footer`
    display: flex;
    justify-content: center;
    align-items: center;
    position: fixed;
    bottom: 0;
    width: 100%;
    height: 125px;
    background-color: ${colors.footerBg};
    background-image: url(${footerBg});
`;

export const FooterContainer = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;

`;

export const PagesSection = styled.div`
    width: 640px;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const PageLink = styled(Link)`
    font-weight: 400;
    font-size: 14px;
    color: ${colors.primaryText};

`;

export const SocialSection = styled.div`
    width: 150px;
    display: flex;
    justify-content: space-between;
    align-items: center;

`;

export const SocialLink = styled.a`
    
`;

export const PhoneSection = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const PhoneLink = styled.a`
    text-decoration: none;
    font-weight: 400;
    font-size: 16px;
    color: ${colors.primaryText};
`;

export const Logo = styled.div`
    display: flex;
    align-items: center;
`;