import React from "react";

import { Img } from "ComponentsRoot";
import {Container} from 'StyledComponentsRoot';
import {FooterSection, FooterContainer, PagesSection, PageLink, Logo, SocialSection, SocialLink, PhoneSection, PhoneLink} from './StyledComponents';

import logoSvg from 'AssetsRoot/img/svg/logo.svg';
import youtube from 'AssetsRoot/img/svg/youtube.svg'
import linkedin from 'AssetsRoot/img/svg/linkedin.svg'
import facebook from 'AssetsRoot/img/svg/facebook.svg'
import instagram from 'AssetsRoot/img/svg/instagram.svg'
import phone from 'AssetsRoot/img/svg/phone.svg'

const Footer = () => {
    return(
        <FooterSection>
            <Container>
                <FooterContainer>
                    <Logo>
                        <Img src={logoSvg} width='40px' height='32px' margin='0 12px 0 0' alt='logo'/>
                    </Logo>
                    <PagesSection>
                        <PageLink to="/about">О компании</PageLink>
                        <PageLink to="/contacts">Контакты</PageLink>
                        {/* <PageLink>Вакансии</PageLink> */}
                        {/* <PageLink>Статьи</PageLink> */}
                        {/* <PageLink>Политика обработки персональных данных</PageLink> */}
                    </PagesSection>
                    <SocialSection>
                        <SocialLink href="">
                            <Img src={instagram} width='24px' height='24px' alt='instagram'/>
                        </SocialLink>
                        <SocialLink href="">
                            <Img src={facebook} width='24px' height='24px' alt='facebook'/>
                        </SocialLink>
                        <SocialLink href="">
                            <Img src={linkedin} width='24px' height='24px' alt='linkedin'/>
                        </SocialLink>
                        <SocialLink href="">
                            <Img src={youtube} width='24px' height='24px' alt='youtube'/>
                        </SocialLink>
                    </SocialSection>
                    <PhoneSection>
                        <Img src={phone} width='24px' height='24px' margin='0 12px 0 0' alt='phone'/>
                        <PhoneLink href="tel:+380635556677">+380635556677</PhoneLink>
                    </PhoneSection>
                </FooterContainer>
            </Container>
        </FooterSection>
    );
}

export default Footer;