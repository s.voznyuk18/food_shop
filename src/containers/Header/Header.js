import React from "react";
import { Link } from "react-router-dom";

import {Container} from 'StyledComponentsRoot';
import { Img, BassicButton, BassicInput } from "ComponentsRoot";
import {Logo, LogoTitle, HeaderSection, HeaderContainer, Menu, MenuBlock, MenuTitle} from './StyledComponents';

import logoSvg from 'AssetsRoot/img/svg/logo.svg';
import favoritesSvg from 'AssetsRoot/img/svg/favorites.svg';
import ordersSvg from 'AssetsRoot/img/svg/orders.svg';
import cartSvg from 'AssetsRoot/img/svg/cart.svg';


import { colors } from "ConfigsRoot/colors";

const Header = ({}) => {
    return (
        <HeaderSection>
            <Container>
                <HeaderContainer>
                    <Logo>
                        <Img src={logoSvg} width='40px' height='32px' margin='0 12px 0 0' alt='logo'/>
                        <LogoTitle>FOOD SHOP</LogoTitle>
                    </Logo>
                    <Link to="/catalog">
                        <BassicButton
                            width='140px'
                            height='40px'
                            display='flex'
                            padding='8px'
                            color={colors.white}
                            backgroundColor={colors.primary}
                        >
                            Каталог
                        </BassicButton>
                    </Link>
                 
                    <BassicInput
                        width='375px'
                        height='40px'
                        padding='8px 8px 8px 16px'
                        fontSize='16px'
                        placeholder='Найти товар'
                        borderRadius='4px'
                        searchInput
                    />

                    <Menu>
                        <MenuBlock>
                            <Img src={favoritesSvg} width='24px' height='24px' alt='favorites'/>
                            <MenuTitle>Избранное</MenuTitle>
                        </MenuBlock>
                        <MenuBlock>
                            <Img src={ordersSvg} width='24px' height='24px' alt='orders'/>
                            <MenuTitle>Заказы</MenuTitle>
                        </MenuBlock>
                        <MenuBlock>
                            <Img src={cartSvg} width='24px' height='24px' alt='cart'/>
                            <MenuTitle>Корзина</MenuTitle>
                        </MenuBlock>
                    </Menu>


                </HeaderContainer>
               
            </Container>
        </HeaderSection>
    );
};

export default Header;