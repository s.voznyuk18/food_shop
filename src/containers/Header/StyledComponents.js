import styled from 'styled-components';

import { colors } from "ConfigsRoot/colors";

export const HeaderSection = styled.header`
    // position: fixed;
    // top: 0;
    width: 100%;
    height: 72px;
    display: flex;
    align-items: center;
    box-shadow: 2px 4px 8px rgba(0, 0, 0, 0.1);
`;

export const HeaderContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;

`;

export const Logo = styled.div`
    display: flex;
    align-items: center;
`;

export const LogoTitle = styled.h1`
    font-size: 16px;
    color: #232323;
    text-transform: uppercase;
`;

export const Menu = styled.div`
    display: flex;
    justify-content: space-between;
`;

export const MenuBlock = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-right: 25px;
`;

export const MenuTitle = styled.div`
    margin-top: 10px;
    color: ${colors.primaryText}
    font-weight: 400;
    font-size: 12px;
`;