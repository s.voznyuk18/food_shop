import styled from "styled-components";

import { colors } from "ConfigsRoot/colors";


export const Container = styled.div`
    width: 1200px;
    margin: 0 auto;
`;

export const Title = styled.h1`
    margin: 0;
    font-weight: 700;
    font-size: 64px;
    color: ${colors.primaryText}
`;