export const colors = {
    primary: '#70C05B',
    secondary: '#FF6633',
    mainBg: '#FBF8EC',
    primaryText: '#414141',
    mediumTest: '#606060',
    lightTesxt: '#BFBFBF',
    inputPlaceholder: '#8F8F8F',
    white: '#FFFFFF',
    footerBg: '#F9F4E2',
}